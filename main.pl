#!/bin/perl

my $command="git clone  git\@gitlab.com:great-war/engine.git './Global War'";
# my $command="git clone --recurse-submodules git\@gitlab.com:great-war/engine.git .";
my $exitCode = system($command);
my $pwd = `pwd | tr -d '\n'`;
my $retiesLimit = 10;
my $reties = 1;

printf("Exit code: %s\n", $exitCode);

while ($exitCode != 0 and $reties <= $retiesLimit) {
  printf("Retry: %s\n", $reties);
  $exitCode = system($command);
  printf("Exit code: %s\n", $exitCode);
  $reties++;
}

if ($exitCode != 0 and $reties >= $retiesLimit) {
  printf("Ошибка при выполнениии команды: '%s'. Код завершения: '%s'. Кол-во попыток: '%s'", $command, $exitCode, $reties);
}

system("cd \"$pwd/Global War\" && ./init.sh");
